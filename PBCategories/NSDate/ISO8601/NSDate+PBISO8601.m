//
//  NSDate+PBISO8601.m
//  PBCategories
//
//  Created by Peter Barclay on 28/09/2014.
//  Copyright (c) 2014 Peter Barclay. All rights reserved.
//

#import "NSDate+PBISO8601.h"

@implementation NSDate (PBISO8601)

+ (NSString *)pb_iso8601StringFromDate:(NSDate *)date timeZone:(NSTimeZone *)timeZone
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)pb_iso8601StringFromDate:(NSDate *)date
{
    return [self pb_iso8601StringFromDate:date timeZone:[NSTimeZone localTimeZone]];
}

- (NSString *)pb_iso8601StringWithTimeZone:(NSTimeZone *)timeZone
{
    return [[self class] pb_iso8601StringFromDate:self timeZone:timeZone];
}

- (NSString *)pb_iso8601String
{
    return [self pb_iso8601StringWithTimeZone:[NSTimeZone localTimeZone]];
}

@end

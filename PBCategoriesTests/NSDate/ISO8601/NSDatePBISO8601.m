//
//  NSDatePBISO8601.m
//  PBCategories
//
//  Created by Peter Barclay on 28/09/2014.
//  Copyright (c) 2014 Peter Barclay. All rights reserved.
//

#import "NSDate+PBISO8601.h"

#import <XCTest/XCTest.h>

#define TESTDATE 1411876177.113 // 2014-09-28T03:49:37Z
#define TESTRESULT_GMT @"2014-09-28T03:49:37Z"
#define TESTRESULT_GMT_MINUS5400 @"2014-09-28T02:19:37-01:30"
#define TESTRESULT_GMT_MINUS3600 @"2014-09-28T02:49:37-01:00"
#define TESTRESULT_GMT_PLUS3600 @"2014-09-28T04:49:37+01:00"
#define TESTRESULT_GMT_PLUS5400 @"2014-09-28T05:19:37+01:30"

#define ONE_HOUR 3600
#define ONE_HOUR_30_MINS 5400

@interface NSDatePBISO8601 : XCTestCase

@end

@implementation NSDatePBISO8601

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - class method tests

- (void)test_pb_iso8601StringFromDateTimeZone_GMT_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [NSDate pb_iso8601StringFromDate:date timeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT);
}

- (void)test_pb_iso8601StringFromDateTimeZone_withPlusOffset_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:ONE_HOUR];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [NSDate pb_iso8601StringFromDate:date timeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_PLUS3600);
}

- (void)test_pb_iso8601StringFromDateTimeZone_withPlusOffsetHalf_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:ONE_HOUR_30_MINS];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [NSDate pb_iso8601StringFromDate:date timeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_PLUS5400);
}

- (void)test_pb_iso8601StringFromDateTimeZone_withMinusOffset_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:-ONE_HOUR];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [NSDate pb_iso8601StringFromDate:date timeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_MINUS3600);
}

- (void)test_pb_iso8601StringFromDateTimeZone_withMinusOffsetHalf_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:-ONE_HOUR_30_MINS];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [NSDate pb_iso8601StringFromDate:date timeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_MINUS5400);
}

- (void)test_pb_iso8601StringFromDate_shouldReturnFormattedDateStringForLocalTimeZone
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [NSDate pb_iso8601StringFromDate:date];
    NSString *expected = [NSDate pb_iso8601StringFromDate:date timeZone:[NSTimeZone localTimeZone]];
    
    XCTAssertEqualObjects(result, expected);
}

#pragma mark - instance method tests

- (void)test_pb_iso8601StringWithTimeZone_GMT_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [date pb_iso8601StringWithTimeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT);
}

- (void)test_pb_iso8601StringWithTimeZone_withPlusOffset_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:ONE_HOUR];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [date pb_iso8601StringWithTimeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_PLUS3600);
}

- (void)test_pb_iso8601StringWithTimeZone_withPlusOffsetHalf_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:ONE_HOUR_30_MINS];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [date pb_iso8601StringWithTimeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_PLUS5400);
}

- (void)test_pb_iso8601StringWithTimeZone_withMinusOffset_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:-ONE_HOUR];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [date pb_iso8601StringWithTimeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_MINUS3600);
}

- (void)test_pb_iso8601StringWithTimeZone_withMinusOffsetHalf_shouldReturnFormattedDateString
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:-ONE_HOUR_30_MINS];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [date pb_iso8601StringWithTimeZone:timeZone];
    
    XCTAssertEqualObjects(result, TESTRESULT_GMT_MINUS5400);
}

- (void)test_pb_iso8601String_shouldReturnFormattedDateStringForLocalTimeZone
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:TESTDATE];
    NSString *result = [date pb_iso8601String];
    NSString *expected = [date pb_iso8601StringWithTimeZone:[NSTimeZone localTimeZone]];
    
    XCTAssertEqualObjects(result, expected);
}

@end

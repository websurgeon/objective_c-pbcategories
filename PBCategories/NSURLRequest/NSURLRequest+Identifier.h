//
//  NSURLRequest+Identifier.h
//  PBCategories
//
//  Created by Peter Barclay on 15/03/2014.
//  Copyright (c) 2014 Peter Barclay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (Identifier)

- (NSString *)identifier;
- (NSString *)identifierShort;

@end

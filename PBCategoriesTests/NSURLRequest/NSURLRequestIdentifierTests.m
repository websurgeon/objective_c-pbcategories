//
//  NSURLRequestIdentifierTests.m
//  PBCategories
//
//  Created by Peter on 15/03/2014.
//
//

#import "NSURLRequest+Identifier.h"

#import "PBCategoriesTestCase.h"

@interface NSURLRequestIdentifierTests : PBCategoriesTestCase

@end

@implementation NSURLRequestIdentifierTests

static NSString * const HTTPMethod_GET = @"GET";
static NSString * const HTTPMethod_POST = @"POST";
static NSString * const URL_ROOT = @"http://peter-barclay.com";
static NSString * const URL_PATH = @"/testing/path";
static NSString * const URL_QUERY = @"?query=something";
static NSString * const HTTPBody_CONTENT = @"somedata";


- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testNSURLRequestIdentifier_identifierIsInstanceOfString
{
    NSURLRequest *sut = [[NSURLRequest alloc] init];
    assertThat([sut identifier], is(instanceOf([NSString class])));
}

- (void)testNSURLRequestIdentifier_identifierGETWithNoQuery
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",URL_ROOT,URL_PATH];
    NSURL *url = [NSURL URLWithString:urlString];

    NSMutableURLRequest *sut = [NSMutableURLRequest requestWithURL:url];

    [sut setHTTPMethod:HTTPMethod_GET];
    
//  expected sha1 string obtained via the following command line expression:
//  $ echo -n "<HTTPMethod_GET> <URL_PATH>" | openssl sha1
    assertThat([sut identifier], is(equalTo(@"b75a7fe92afc2ebe774e717bd599dc36c0457236")));
}

- (void)testNSURLRequestIdentifier_identifierGETWithQuery
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",URL_ROOT,URL_PATH,URL_QUERY];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *sut = [NSMutableURLRequest requestWithURL:url];
    
    [sut setHTTPMethod:HTTPMethod_GET];
    
//  expected sha1 string obtained via the following command line expression:
//  $ echo -n "<HTTPMethod_GET> <URL_PATH><URL_QUERY>" | openssl sha1
    assertThat([sut identifier], is(equalTo(@"7621d339ce7b89a7a7f7ba83008cb5b8fa45887d")));
}

- (void)testNSURLRequestIdentifier_identifierPOSTWithQueryANDBody
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",URL_ROOT,URL_PATH,URL_QUERY];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *sut = [NSMutableURLRequest requestWithURL:url];
    
    [sut setHTTPMethod:HTTPMethod_POST];
    NSData *payload = [HTTPBody_CONTENT dataUsingEncoding:NSUTF8StringEncoding];
    [sut setHTTPBody:payload];
    
    //  expected sha1 string obtained via the following command line expression:
    //  $ echo -n "<HTTPMethod_POST> <URL_PATH><URL_QUERY><[payload description]>" | openssl sha1
    assertThat([sut identifier], is(equalTo(@"28fea2090e3a10197987d7e243d73b0ec4f636c5")));
}

- (void)testNSURLRequestIdentifier_hashIdentifierShort
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",URL_ROOT,URL_PATH];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *sut = [NSMutableURLRequest requestWithURL:url];
    
    [sut setHTTPMethod:HTTPMethod_GET];
    
    //  expected sha1 string obtained via the following command line expression:
    //  $ echo -n "<HTTPMethod_GET> <URL_PATH>" | openssl sha1
    assertThat([sut identifierShort], is(equalTo(@"b75a7f")));
}

- (void)testNSURLRequestIdentifier_hashIdentifierREADMEexample
{
    NSURL *url = [NSURL URLWithString:@"https://bitbucket.org/websurgeon/objective_c-pbcategories/overview"];
    
    NSMutableURLRequest *sut = [NSMutableURLRequest requestWithURL:url];
    
    [sut setHTTPMethod:HTTPMethod_GET];
    
    //  expected sha1 string obtained via the following command line expression:
    //  echo -n "GET /websurgeon/objective_c-pbcategories/overview" | openssl sha1
    assertThat([sut identifier], is(equalTo(@"54854c4944f878031f8a2ed883becf6ece5188d0")));
}

@end

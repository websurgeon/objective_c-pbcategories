//
//  NSURLRequest+Identifier.m
//  PBCategories
//
//  Created by Peter Barclay on 15/03/2014.
//  Copyright (c) 2014 Peter Barclay. All rights reserved.
//

#import "NSURLRequest+Identifier.h"

#import <CommonCrypto/CommonDigest.h>

@implementation NSURLRequest (Identifier)

static NSInteger const kNSURLRequestIdentifierShortLength = 6;

- (NSString *)identifier
{
    return [self sha1ForString:[self prehashedIdentifier]];
}

- (NSString *)identifierShort
{
    return [[self identifier] substringToIndex:kNSURLRequestIdentifierShortLength];
}

- (NSString *)identifierName;
{
    return [NSString stringWithFormat:@"%@ %@",self.HTTPMethod,self.URL.path];
}

- (NSString *)prehashedIdentifier
{
    return [NSString stringWithFormat:@"%@%@%@%@",
            [self identifierName],
            self.URL.query ? @"?" : @"",
            self.URL.query ?: @"",
            self.HTTPBody ? [self.HTTPBody description] : @""];
}

// sha1 string hashing algorithm implemented using solution by Saurabh
// http://www.makebetterthings.com/iphone/how-to-get-md5-and-sha1-in-objective-c-ios-sdk/
// Dependency: <CommonCrypto/CommonDigest.h>
- (NSString *)sha1ForString:(NSString *)input
{
    NSData *data = [input dataUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes,(CC_LONG)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

@end

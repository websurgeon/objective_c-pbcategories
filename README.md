## A repo to manage categories

#### Setup 

This project uses OCHamcrest framework for testing.  The framework can be retrieved by running a pod install ...

```sh
$ cd <path-to-repo>
$ pod install
```


----
### NSURLRequest+Identifier

This category was created for use in a project where mock json response files were needed. This category provides a way of deterministically generating an identifier for requests so that mock response file names can be generated.

e.g. a post request with some postData sent to http://example-api.com/MyAPIEndPoint/SomeMethod?aquery=avalue would produce an identifier that is based on the request method, url path, url query string and post data.
The identifierShort could then be used to generate a mock response file name like:

```
'POST /MyAPIEndPoint/SomeMethod/ a58bc2.json'
```

#### identifier

returns sha1 hash identifier of an NSURLRequest based on its request method, path, query string and body payload.

e.g. for a GET request to 'https://bitbucket.org/websurgeon/objective_c-pbcategories/overview'
```
54854c4944f878031f8a2ed883becf6ece5188d0
// $ echo -n "GET /websurgeon/objective_c-pbcategories/overview" | openssl sha1

```

The hash is generated to match:

```
$ echo -n "<HTTPMethod_POST> <URL_PATH><URL_QUERY><[payload description]>" | openssl sha1
```

#### identifierShort

returns first 6 chars of identifier

e.g.
```
54854c
```

----
### NSDate+PBISO8601

Provides class and instance methods to return [ISO8601](http://www.iso.org/iso/iso8601) formatted string for NSDate objects.

Usage:

```
// Class method with timeZone
#define aDate [NSDate date]
#define aTimeZone [NSTimeZone localTimeZone]
NSString *formattedDateString1 = [NSDate pb_iso8601StringFromDate:aDate timeZone:aTimeZone];

// Class method without timeZone (uses localTimeZone)
NSString *formattedDateString2 = [NSDate pb_iso8601StringFromDate:aDate];

```

```
// Instance method with timeZone
#define aDate [NSDate date]
#define aTimeZone [NSTimeZone localTimeZone]
NSString *formattedDateString1 = [aDate pb_iso8601StringWithTimeZone:aTimeZone];

// Instance method without timeZone (uses localTimeZone)
NSString *formattedDateString2 = [aDate pb_iso8601String];
```

----
### The MIT License

> Copyright (C) 2014 Peter Barclay.
>
> Permission is hereby granted, free of charge, to any person
> obtaining a copy of this software and associated documentation files
> (the "Software"), to deal in the Software without restriction,
> including without limitation the rights to use, copy, modify, merge,
> publish, distribute, sublicense, and/or sell copies of the Software,
> and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
>
> The above copyright notice and this permission notice shall be
> included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
> NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
> BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
> ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
> CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
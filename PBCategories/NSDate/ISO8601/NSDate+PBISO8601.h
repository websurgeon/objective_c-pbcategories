//
//  NSDate+PBISO8601.h
//  PBCategories
//
//  Created by Peter Barclay on 28/09/2014.
//  Copyright (c) 2014 Peter Barclay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (PBISO8601)

+ (NSString *)pb_iso8601StringFromDate:(NSDate *)date timeZone:(NSTimeZone *)timeZone;
+ (NSString *)pb_iso8601StringFromDate:(NSDate *)date;

- (NSString *)pb_iso8601StringWithTimeZone:(NSTimeZone *)timeZone;
- (NSString *)pb_iso8601String;

@end
